package gnukhata.controllers.reportmodels;

public class ProfitAndLossReport {
	private String to;
	private String accountName;
	private String amount;
	private String by;
	private String accountName1;
	private String amount1;
	public ProfitAndLossReport(String to, String accountName, String amount, String by,
			String accountName1, String amount1){
		super();
		this.to=to;
		this.accountName=accountName;
		this.amount=amount;
		this.by=by;
		this.accountName1=accountName1;
		this.amount1=amount1;
	}
	

	public String getto() {
		return to;
	}
	
	public String getaccountname() {
		return accountName;
	}
	
	public String getamount() {
		return amount;
	}
	
	public String getby() {
		return by;
	}
	
	public String getaccountname1() {
		return accountName1;
	}
	
	public String getamount1() {
		return amount1;
	}
	
	

}


